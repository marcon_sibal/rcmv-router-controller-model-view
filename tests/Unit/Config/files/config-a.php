<?php
return [
    'filename' => 'config-a',
    'multi-layer' => [
        'layer1' => [
            'layer2' => [
                'layer3' => [
                    'content' => 'test-content',
                    'title' => 'test-title'
                ]
            ]
        ]
    ]
];
