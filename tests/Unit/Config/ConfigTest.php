<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use RCMV\Config\Config;

final class ConfigTest extends TestCase
{
    /**
     * Initialize the config
     */
    public function __construct()
    {
        parent::__construct();

        Config::clearConfig();
        Config::loadFromDir(__DIR__ . '/files/');
    }

    /**
     * Check if loading file twice throws an exception.
     */
    public function testCannotLoadFilesWithSameName(): void
    {
        $this->expectException(Exception::class);

        Config::loadFromDir(__DIR__ . '/files/');
    }

    /**
     * Check the configuration is loaded from multiple files.
     */
    public function testCanLoadFromDifferentFiles(): void
    {
        $this->assertEquals('config-a', Config::getConfig('config-a.filename'));
        $this->assertEquals('config-b', Config::getConfig('config-b.filename'));
    }

    /**
     * Check if getConfig can access multi-dimensional arrays.
     */
    public function testCanAccessMultiLayerConfig(): void
    {
        $this->assertEquals('test-content', Config::getConfig('config-a.multi-layer.layer1.layer2.layer3.content'));
    }

    /**
     * Check getConfig can return array values.
     */
    public function testCanRetriveConfigurationArrayValues(): void
    {
        $expected = [
            'layer1' => [
                'layer2' => [
                    'layer3' => [
                        'content' => 'test-content',
                        'title' => 'test-title'
                    ]
                ]
            ]
        ];

        $this->assertEquals($expected, Config::getConfig('config-a.multi-layer'));
    }

    /**
     * Check if getConfig returns null if no matching key is found.
     */
    public function testCheckNoConfigurationMatchReturnsNull(): void
    {
        $this->assertEquals(null, Config::getConfig('no-config.match'));
    }

}
