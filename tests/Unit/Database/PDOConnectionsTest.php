<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use RCMV\Database\PDOConnections;
use PDOException;
use PDO;

final class PDOConnectionsTest extends TestCase
{
    /**
     * Check the method getInstance is return a PDOConnection Instance.
     */
    public function testCheckGetInstanceReturnsPdoConnectionInstance()
    {
        $this->assertInstanceOf(PDOConnections::class, PDOConnections::getInstance());
    }

    /**
     * Check if getInstance method returns the same instance when called multiple times.
     */
    public function testCheckGetInstanceReturnsSameInstance()
    {
        $pdo1 = PDOConnections::getInstance();
        $pdo2 = PDOConnections::getInstance();

        $this->assertSame($pdo1, $pdo2);
    }

    public function testCheck_GetConnection_Method_no_driver_throws_an_error()
    {
        $this->expectException(PDOException::class);
        $pdoConnections = PDOConnections::getInstance();

        $dbConfig = [
            'db'        => 'test',
            'host'      => 'test',
            // 'driver'    => 'test',
            'username'  => 'test',
            'password'  => 'test'
        ];

        //turn on debug
        $pdoConnections->Debug(true);
        $pdoConnections->getConnection('test', $dbConfig);
    }

}
