<?php
return [
    'default' => 'integration-test',
    'integration-test' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'db' => 'integration-test',
        'username' => 'root',
        'password' => 'vagrant'
    ]
];