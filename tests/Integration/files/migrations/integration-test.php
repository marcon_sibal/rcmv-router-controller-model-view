<?php
declare(strict_types=1);

require_once __DIR__ . '/../../../../src/Config/Config.php';

use PDO;
use PDOException;
use RCMV\Config\Config;

Config::loadFromDir(__DIR__ . '/../config/');

$dbConfig = Config::getConfig('models-config.db');

var_dump($dbConfig);
// $pdo = new PDO();