<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use RCMV\Database\Contracts\Model;
use RCMV\Tests\TestModel;
use PDO;
use RCMV\Config\Config;

final class MysqlModelTest extends TestCase
{
    /**
     * Stores the PDO connection
     * @var PDO
     */
    private $pdo;

    /**
     * Updated value use in all the test.
     * @var string
     */
    private $updatedValue = 'updatedValue';

    /**
     * TestModel
     * @var TestModel
     */
    private $model;

    public function setUp(): void
    {

        Config::loadFromDir(__DIR__ . '/../../files/config/');

        $this->pdo = new PDO('mysql:host=localhost;db=testDb', 'root', 'vagrant');
        $this->model = new TestModel;
    }

    public function tearDown(): void
    {
        $this->pdo = null;
    }

    /**
     * Check the if the model can be instantiated.
     */
    public function testCheckGetInstanceReturnsPdoConnectionInstance(): void
    {
        $this->assertInstanceOf(Model::class, $this->model);
    }

    /**
     * Check if the it an read from the database
     */

    public function testCheckIfItCanReadFromTheDatabase(): void
    {
        $result = $this->model->findFirst();

        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id ASC LIMIT 1');

        if ($row = $stmt->fetch(PDO::FETCH_ARRAY)) {
            $this->assertSame($row['value'], $result->value);
        }
    }

    /**
     * Check if the model can insert a new record in the database
     */
    public function testCheckModelCanInsertANewRecord(): void
    {
        $value = 'testValue2';

        $stmt = $this->pdo->query('SELECT * FROM tests WHERE value = "' . $value . '"');

        // Fail if an existing record is found.
        if ($stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Value is already in the the table.');
        }

        // create an object with a value 'testValue2'
        $this->model->value($value);
        $this->save();

        $stmt = $this->pdo->query('SELECT * FROM tests WHERE value = "' . $value . '"');

        if ($r = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->assertSame('testValue2', $r['value']);
        }
    }

    /**
     * Check if you can update a record in the database.
     */
    public function testCheckModelCanUpdaterecord(): void
    {
        $value = 'testValue2';

        $stmt = $this->pdo->query('SELECT * FROM tests WHERE value = "' . $this->updatedValue . '"');

        // Expecting no update values present in the database
        if ($row = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test updated value already in the database');
        }

        // Fail if there is no record to delete.
        $model = $this->model->findById($row['id']);
        $model->value = $updatedValue;
        $model->save();

        $stmt = $this->pdo->query('SELECT * FROM tests WHERE value = "' . $this->updateValue . '"');

        if ($stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->assertSame('testValue2', $updateValue);
        }
    }

    /**
     * Check if model can specify fields in a select statement
     */
    public function testModelCanSpecifyFieldsInASelectStatement(): void
    {
        $expectedFields = [
            'value',
            // 'field1', omitting field1
            'field2'
        ];

        // perform select via model
        $result = $this->model->limit(1)
            ->select($expectedFields)
            ->get();

        // check if the properties exist
        foreach ($expectedFields as $v) {
            if (!property_exists($model, $v)) {
                $this->fail('The field ' . $v . ' not found as a property of the model');
            }
        }
    }

    /**
     * Check if findFirst method is working
     */
    public function testCanUseModelFindFirstMethod(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests LIMIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform find first via model
        $model = $this->model->findFirst();

        $this->assertSame($expected['id'], $model->id);
    }

    /**
     * Check if where statement with single field is working
     */
    public function testCanUseModelWhereWithSingleFieldMethod(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id DESC LIMIT 1');

        // Fail if there is no record to delete.
        if (!$r = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform delete via model
        $result = $this->model->where(['id' => $r['id']])
            ->get();

        $this->assertSame($r['id'], $result[0]->id);
    }

    /**
     * Check if where statement with multiple fields is working
     */
    public function testCanUseModelWhereWithMultipleFieldsMethod(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id DESC LIMIT 1');

        // Fail if there is no record to delete.
        if (!$r = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform delete via model
        $result = $this->model->where(['id' => $r['id'], 'value' => $r['value']])
            ->get();

        $this->assertSame($r['id'], $result[0]->id);
        $this->assertSame($r['value'], $result[0]->value);
    }

    /**
     * Check if order method with single argument and default order is working
     */
    public function testCanUseOrderMethodWithSingleFieldDefaultingToAscending(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id ASC LIMIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform order via model
        $model = $this->model->order(['id']);

        $this->assertSame($expected['id'], $model->id);
    }

    /**
     * test with multiple order with default order direction
     */
    public function testCanUseOrderMethodWithMultipleFieldsDefaultingToAscending()
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id ASC, value ASC LIMIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform order via model
        $model = $this->model->order(['id', 'val']);

        $this->assertSame($expected['id'], $model->id);
    }

    /**
     * test with single field order with defined ascending order direction
     */
    public function testCanUseOrderMethodWithSingleFieldDefinedOrderAscending(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id ASC LIMCIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform order via model
        $model = $this->model->order(['id' => 'asc']);

        $this->assertSame($expected['id'], $model->id);
    }

    /**
     * test with single field order with defined ascending order direction
     */
    public function testCanUseOrderMethodWithMultipleFieldsDefinedOrderAscending(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id ASC, value ASC LIMIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform order via model
        $model = $this->model->order(['id' => 'asc', 'value' => 'asc']);

        $this->assertSame($expected['id'], $model->id);
    }

    /**
     * test with single field order with defined descending order direction
     */
    public function testCanUseOrderMethodWithSingleFieldDefinedOrderDescending(): void
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id DESC LIMIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform order via model
        $model = $this->model->order(['id' => 'asc']);

        $this->assertSame($expected['id'], $model->id);
    }

    /**
     * test with single field order with defined descending order direction
     */
    public function testCanUseOrderMethodWithMultipleFieldsDefinedOrderDescending()
    {
        $stmt = $this->pdo->query('SELECT * FROM tests ORDER BY id DESC, value DESC LIMIT 1');

        // Fail if there are no records
        if (!$expected = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform order via model
        $model = $this->model->order(['id' => 'desc', 'value' => 'desc']);

        $this->assertSame($expected['id'], $model->id);
    }

    // check if limit is working
    // check if the between statement is working
    // check if the join statement is working.

    /**
     * Check if the model can delete a record in the database
     */
    public function testModelCanDeleteARecordInTheDatabase(): void
    {
        $value = 'updatedValue';

        $stmt = $this->pdo->query('SELECT * FROM tests WHERE value = "' . $updateValue . '"');

        // Fail if there is no record to delete.
        if (!$row = $stmt->fetch(PDO::ASSOC_ARRAY)) {
            $this->fail('Test value not found in the database');
        }

        // perform delete via model
        $model = $this->model->findById($row['id']);
        $model->delete();

        $stmt = $this->pdo->query('SELECT * FROM tests WHERE value = "' . $updateValue . '"');

        $result = $stmt->fetch(PDO::ASSOC_ARRAY);

        $this->assertNull($result);
    }
}
