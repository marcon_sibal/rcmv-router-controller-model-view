<?php
declare(strict_types=1);

namespace RCMV\Database\Contracts;

interface Database
{
    /**
     * Returns the instance of the Database.
     *
     * @return [type] [description]
     */
    public static function getInstance();

    /**
     * Returns a database connection.
     *
     * @param  string $dbConfigName  : Name in the configuration file
     * @param  array  $dbCredentials : Database credentials
     *
     * @return PDO
     */
    public function getConnection(string $dbConfigName, array $dbCredentials);

}