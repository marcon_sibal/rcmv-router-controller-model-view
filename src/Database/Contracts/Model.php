<?php
declare(strict_types=1);

namespace RCMV\Database\Contracts;

interface Model
{
    /**
     * Static method that finds a record from the db and
     * sets the values of the column to its respective property
     *
     * @param int    $id
     *
     * @return A model with properties created from result.
     */
    // public static function find(int $id);

    // /**
    //  * Finds a record by primary key
    //  *
    //  * @param  int    $id
    //  * @return A new instance of a model with properties set from the query result
    //  */
    // public function findById(int $id);

    // /**
    //  * Update the represent record
    //  */
    // public function update();

    // /**
    //  * Insert a record derived on this model
    //  */
    // public function save();

    // /**
    //  * Deletes record
    //  */
    // public function delete();

    // /**
    //  * Sets the $selectFields
    //  */
    // public function select(string $select);

    // *
    //  * Sets the limit and offset of an sql statement

    // public function limit(int $limit, in $offset = null);

    // /**
    //  * Creates the where statement
    //  */
    // public function where(string $field, string $operator = '=', $value);

    // /**
    //  * Return an array on model instance
    //  */
    // public function get();

    // /**
    //  * Returns row as an array
    //  */
    // public function getArray();

    // /**
    //  * Returns result as rows of array having
    //  * a specified column as its key
    //  *
    //  * @param  string $columnName
    //  * @return [type]
    //  */
    // public function getArrayWithKey(string $columnName);

}
