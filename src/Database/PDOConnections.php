<?php
declare(strict_types=1);

namespace RCMV\Database;

use RCMV\Database\Contracts\Database as DatabaseContract;
use RCMV\Config\Config;
use PDOException;
use PDO;
/**
 * PDOConnections is a singleton that handles the PDO connections.
 * It stores the connection in an array by name. This way it only
 * connects once for each database configuration.
 */
class PDOConnections implements DatabaseContract
{
    /**
     * The created instance through the getInstance.
     *
     * @var Msibal\MsRCMV\Database\Database;
     */
    protected static $instance;

    /**
     * An array of PDO connections.
     *
     * @var array
     */
    protected $connections = [];

    /**
     * Not meant to be instantiated directly.
     */
    private function __construct()
    {
    }

    /**
     * Destroys the connections after use.
     */
    public function __destruct()
    {
        $this->connections = null;
    }

    /**
     * Instantiates PDOConnection.
     *
     * @return RCMV/Database/PDOConnections
     */
    public static function getInstance() : self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Returns the PDO connection.
     *
     * @return PDO
     */

    /**
     * Returns the PDO connection if connection already exist otherwise
     * create a connection.
     *
     * @param  string $dbConfigName  : Name in the configuration file
     * @param  array  $dbCredentials : Database credentials
     *
     * @return PDO
     *
     * @return PDO
     */
    public function getConnection(string $dbConfigName, array $dbCredentials) : PDO
    {
        if (!isset($this->connections[$dbConfigName])) {
            // if the connection is not existing create a connection and store it.
            $this->connect($dbConfigName, $dbCredentials);
        }

        return $this->connections[$dbConfigName];
    }

    /**
     * This method will attempt to connect to the database using the given
     * database credential. If it fails it will throw an error.
     *
     * @param  string $dbConfigName  : Name in the configuration file
     * @param  array  $dbCredentials : Database credentials
     *
     * @return void
     *
     * @todo  Support for the sqlite, probably create a method that will generate the
     *        connection string.
     */
    protected function connect(string $dbConfigName, array $dbCredentials) : void
    {
        try {
            $this->connections[$dbConfigName] = new PDO(
                ($dbCredentials['driver'] ?? '') . ":" .
                (isset($dbCredentials['host']) ? "host={$dbCredentials['host']};" : "") .
                (isset($dbCredentials['db']) ? "dbname={$dbCredentials['db']};" : "") .
                (isset($dbCredentials['port']) ? "port={$dbCredentials['port']};" : ""),
                $dbCredentials['username'],
                $dbCredentials['password']
            );

            $this->connections[$dbConfigName]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            // @todo: improve error handling
            throw $e;
            exit();
        }
    }
}
