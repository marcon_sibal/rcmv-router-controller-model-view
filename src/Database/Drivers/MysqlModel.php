<?php
declare(strict_types=1);

namespace RMVC\Database\Drivers;

use RMVC\Database\Model;
use PDO;

abstract class MysqlModel
{
    protected $model;

    public function __construct(Model $model)
    {
       $this->model = $model;
    }

    /**
     * Finds a record by primary key
     *
     * @param  int    $id
     * @return A new instance of a model with properties set from the query result
     */
    public function findById(int $id)
    {
        $model = $this->model;
        $sql = "SELECT * FROM {$this->table} WHERE {$this->primaryKey} = {$id} LIMIT 1";
        $stmt = $model->pdo->query($sql);

        if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //store the fields for update reference;
            $model->tableFields = $row;

            foreach ($row as $k => $v) {
                $model->{$k} = $v;
            }

            return $mdoel;
        }

        return false;
    }

    /**
     * Update the represent record
     */
    public function update()
    {
        $updateString = "";
        $updateData = [];

        foreach ($this->tableFields as $k => $v) {
            if ($v !== $this->{$k}) {
                $updateString .= ", {$k}=:{$k}";
                //store the value from the model objects
                $updateData[':' . $k] = $this->{$k};
            }
        }

        if ($updateString !== "") {
            $updateString = substr($updateString, 2);
            $updateData[':primarykey'] = $this->{$this->primaryKey};

            $sql = "UPDATE {$this->table} SET {$updateString} WHERE {$this->primaryKey}=:primarykey";

            $stmt = $this->pdo->prepare($sql);

            return $stmt->execute($updateData);
        }

        return false;
    }

    /**
     * Insert a record derived on this model
     */
    public function save() {

        $fieldString = "";
        $valueString = "";
        $insertData = [];

        foreach ($this->tableFields as $k => $v) {

            if (isset($this->{$k}) && $k !== $this->primaryKey) {
                $fieldString .= ", {$k}";
                $valueString .= ", :{$k}";

                $insertData[':' . $k] = $this->{$k};
            }
        }

        if ($fieldString !== "") {
            $fieldString = substr($fieldString, 2);
            $valueString = substr($valueString, 2);

            $sql = "INSERT INTO {$this->table} ({$fieldString}) VALUE ({$valueString});";
            $stmt = $this->pdo->prepare($sql);

            if ($stmt->execute($insertData)) {
                return $this->pdo->lastInsertId();
            }
        }

        return false;
    }

    /**
     * Deletes record
     */
    public function delete()
    {
        $stmt = $this->pdo->prepare("DELETE FROM {$this->table} WHERE {$this->primaryKey} = :{$this->primaryKey}");
        return $stmt->execute([":{$this->primaryKey}" => $this->{$this->primaryKey}]);
    }

    /**
     * Sets the $selectFields
     */
    public function select(string $select)
    {
        $this->selectFields = $select;

        return $this;
    }

    /**
     * Sets the limit and offset of an sql statement
     */
    public function limit(int $limit, in $offset = null)
    {
        $this->selectLimit = $limit;
        $this->selectOffset = $offset;

        return $this;
    }

    /**
     * Creates the where statement
     */
    public function where(string $field, string $operator = '=', $value)
    {
        $WhereAnd = ($this->whereString === '') ? 'WHERE' : 'AND';
        $valueKey = count($this->whereData);
        $this->whereString .= " " . ($this->whereString === '' ? 'WHERE' : 'AND')
            . " {$field} {$operator} :where{$valueKey}";

        $this->whereData[':where' . $valueKey] = $value;

        return $this;
    }

    /**
     * Build the query statement
     *
     * @todo  improve the result make it uniformed.
     * OR separate the array result;
     */
    private function getQueryStatement()
    {
        $sql = "SELECT {$this->selectFields} FROM {$this->table}"
            . (($this->whereString !== '') ? " " . $this->whereString : '')
            . " LIMIT " . $this->selectLimit
            . (($this->selectOffset !== null) ? " OFFSET " . $this->selectOffset : '');

        $stmt = $this->pdo->prepare($sql);
        if (!$stmt->execute($this->whereData)) {
            return fasle;
        }

        return $stmt;
    }

    /**
     * return an array on model instance
     */
    public function get()
    {
        $stmt = $this->getQueryStatement();

        $result = [];
        $class = static::class;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $model = new $class;

            $model->tableFields = $row;

            foreach ($row as $k => $v) {
                $model->{$k} = $v;
            }

            $result[] = $model;
        }

        return $result;
    }

    /**
     * returns row as an array
     */
    public function getArray()
    {
        $stmt = $this->getQueryStatement();
        $result = [];

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $result[] = $row;
        }

        return $result;
    }

    /**
     * Returns result as rows of array having
     * a specified column as its key
     * @param  string $columnName
     * @return [type]
     */
    public function getArrayWithKey(string $columnName)
    {
        $stmt = $this->getQueryStatement();
        $result = [];

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $result[$row[$key]] = $row;
        }

        return $result;
    }
}
