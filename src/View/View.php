<?php

class View
{
    /**
     * Data to be passed on the view.
     * @var array
     */
    private $data = [];

    /**
     * Render
     * @param  [type] $path [description]
     * @return [type]       [description]
     */
    public function render($path)
    {
        ob_start();

        extract($this->data);

        try {
            include $path;
        } catch (Exception $e) {
            echo $->getMessage();
        }

        return trim(ob_get_clean());
    }

    /**
     * storeData
     * @param  array  $data [description]
     * @return [type]       [description]
     */
    public function storeData(array $data = []) : View
    {
        $this->data += $data;

        return $this;
    }
}