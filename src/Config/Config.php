<?php
declare(strict_types=1);
namespace RCMV\Config;

use Exception;

/**
 * Config loads configuration from a php file.
 *
 * @todo
 *     - LoadFrom($file, $alias): method to load specific file that can
 *         optionally store it under an alias config name.
 *     - OvewriteConfig($file): method to load configuration and overwrite
 *         existing values.
 */
class Config
{
    /**
     * Storage for the loadeArray
     *
     * @var array
     */
    private static $configCache = [];

    /**
     * Not meant to be instantiated
     */
    private function __construct()
    {
    }

    /**
     * Loads from configuration from multiple files. Loading the same files with the name
     * is not allowed to avoid accidentally overwriting configuration.
     *
     * @param string|null $configPath: use this to replace
     *     the default configuration file path.
     *
     * @return void
     */
    public static function loadFromDir(string $configPath = 'config/') : void
    {
        if (file_exists($configPath)) {
            $configFiles = array_diff(scandir($configPath), ['.', '..']);

            foreach ($configFiles as $file) {
                $configName = str_replace('.php', '', $file);

                if (!array_key_exists($configName, self::$configCache)) {
                    self::$configCache[$configName] = include($configPath . $file);
                } else {
                    throw new Exception("Cannot load configuration file '{$file}'.
                        \r\n A configuration file with the same name has already been loaded.");
                }
            }
        } else {
            throw new Exception("Configuation file path does not exist.");
        }
    }

    /**
     * Retrieves the value from the configuration file.
     *
     * @param  string $configPath [path separated by "."]
     *
     * @return mixed
     */
    public static function getConfig(string $configPath)
    {
        $config = self::$configCache;

        foreach (explode('.', $configPath) as $path) {
            if (!isset($config[$path])) {
                return null;
            }

            $config = $config[$path];
        }

        return $config;
    }

    /**
     * Clears the configuration storage.
     * NOTE: The is use to clear the config to avoid throwing exception
     * when a filename is loaded twice.
     */
    public static function clearConfig() : void
    {
        self::$configCache = [];
    }
}